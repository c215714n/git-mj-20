//Navigation
    menuBtn = document.querySelector('nav .btn')
    menuLst = document.querySelector('nav .menu')

    menuBtn.onclick = () => menuLst.classList.toggle('active') ? menuBtn.innerHTML = '&times;' : menuBtn.innerHTML = '&equiv;'
//Background Map
    mapFrm = document.querySelector('#footer iframe')
    mapBtn = document.querySelector('#footer .btn')

    mapBtn.addEventListener( 'click', () => { 
        mapFrm.classList.toggle('active')
        mapBtn.classList.toggle('active')
    }   )
// Mode
    modeChk = document.querySelector('#mode')
    modeLbl = document.querySelector('nav label')
    modeTag = document.querySelector('body')
    modeChk.oninput = () => modeTag.classList.toggle('dark')
